use aw_errors::AWError;

/// Trait to be implemented by each application
pub trait Application {
    type Message;

    /// Create a new application
    fn new() -> Result<Self, AWError>
    where
        Self: Sized;

    /// Run the main loop for an application
    fn run(self) -> Result<i32, AWError>;
}
